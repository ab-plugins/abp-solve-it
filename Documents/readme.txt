## Changelog ##

* version 0.1 - 01/10/2018
Initial release
* version 0.2 - 03/10/2018
Added option to add aprefix to the thread
* version 0.3 - 08/10/2018
Added fine tuning : it lets owner to override the global settings for particular forums.

## Installation
* Upload the content of the Upload/ directory to your forum root
* Install and activate the plugin
* Change settings. The default values forbid all forums to be solved

## Customization
The plugin creates stylesheet (abp_solveit.css) that you can edit using the stylesheet editor in the ACP.
Actually, there is only 2 elements styled:
- the "solve" button
- the post_content of the solution