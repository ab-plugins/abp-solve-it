# ABP Solve It

This plugin allows some people to mark a thread as solved

## Installation
* Upload the content of the Upload/ directory to your forum root
* Install and activate the plugin
* Change settings. The default values forbid all forums to be solved

## Customization
The plugin creates stylesheet (abp_solveit.css) that you can edit using the stylesheet editor in the ACP.
Actually, there is only 2 elements styled:
- the "solve" button
- the post_content of the solution