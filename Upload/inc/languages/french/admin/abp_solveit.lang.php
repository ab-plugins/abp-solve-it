<?php
$l['abp_siname'] = 'ABP Solve It !';
$l['abp_sidesc'] = 'Un moyen de passer les sujets en résolus';
$l['abp_sigs'] = 'ABP Solve It ! - Réglages globaux';
$l['abp_sigsd'] = 'Réglages globaux pour ABP Solve It !';
$l['abp_sis1_tit'] = 'Forums';
$l['abp_sis1_desc'] = 'Forums où le module est actif';
$l['abp_sis2_tit'] = 'Groupes';
$l['abp_sis2_desc'] = 'Groupes de membres pouvant résoudre un sujet';
$l['abp_sis3_tit'] = 'Initiateur peut résoudre';
$l['abp_sis3_desc'] = 'L\'initiateur d\'un sujet peut-il choisir une solution sans être dans un groupe autorisé ?';
$l['abp_sis4_tit'] = 'Fermer le sujet';
$l['abp_sis4_desc'] = 'Le sujet doit-il être fermé lorsqu\'il est résolu ?';
$l['abp_sis4_tit'] = 'Préfixe à appliquer';
$l['abp_sis4_desc'] = 'Appliquer un préfixe à la discussion ?';

$l['abp_sijs1'] = 'Choisir comme solution';
$l['abp_sijs2'] = 'Aller à la solution';

// ADMIN PAGE
$l['abp_siadmin_action'] = 'Réglages fins ABP Solve It !';
$l['abp_siadmin_form'] = 'Changer les options';
$l['abp_siadmin_save'] = 'Sauver';
$l['abp_siadmin_noforum'] = 'Aucun forum ne pouvant être géré';
$l['abp_siadmin_updok'] = 'Vos préférences sont à jour';
$l['abp_siadmin_rempfx'] = 'Supprimer le préfixe';