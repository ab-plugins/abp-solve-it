<?php
// Javascript languages
$l['abp_sijs3'] = 'Il y a eu une erreur avec la résolution :';

// Errors
$l['abp_sierr1'] = 'Erreur d\'authentification';
$l['abp_sierr2'] = 'Ce forum n\'accepte pas les solutions';
$l['abp_sierr3'] = 'Vous n\'êtes pas autorisé à résoudre ce sujet';